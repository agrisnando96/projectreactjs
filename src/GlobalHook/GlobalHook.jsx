import React from 'react';
// import createPersistedState from '@plq/use-persisted-state';
import globalHook from 'use-global-hook';


    const initialState = {
        counter: 0,
    }

    const actions = {
        addToCounter: (store,amount) => {
            const newCounterValue = store.state.counter + amount;
            store.setState({ counter: newCounterValue })
        },
    };

    const useGlobal = globalHook(React,initialState,actions);

     const GlobalHook = () => {
        const [globalState,globalActions] = useGlobal();
        return (
            <div className="App">
                <p>
                    counter:
                    {globalState.counter}
                </p>
                <button type="button" onClick={()=> globalActions.addToCounter(1)}>
                    +1 to Gloval
                </button>
            </div>
        )
    }

    export default GlobalHook