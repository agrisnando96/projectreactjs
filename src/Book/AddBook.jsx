import React, { useState, useEffect } from 'react';
import axios from 'axios';
import moment from 'moment';
import { useForm } from 'react-hook-form'
import App from '../App'


const Create = (props) => {
	const url = "http://127.0.0.1:8080/api/books"
	const roles = sessionStorage.getItem("roles");

	const [form, setForm] = useState({
		title: "",
		author: "",
		published_date: "",
		pages: "",
		language: "",
		publisher_id: ""
	})




	const Submit = () => {
		axios.post(url, form, {
			headers: {
				"Authorization": sessionStorage.getItem('token')
			}
		})
			.then(res => {
				console.log(res.data)
				alert("data buku berhasil ditambahkan")
				props.history.push("/book")
			}).catch(err => console.error(err))
	}

	const handleChange = (e) => {
		const { name, value } = e.target
		setForm({
			...form,
			[name]: value
		})
	}
	// const newData={...form}
	// newData[e.target.id] = e.target.value
	// setForm(newData)

	const { register, handleSubmit, errors } = useForm({
		mode: "onChange"
	});

	return (
		<>
			{roles == 2 ?
				<div className="container col-10">
				<div className="row mt-5">
				<div className="col-7">
				<div className="card w-responsive">
					<div className="card-header">
						Tambah Buku
					</div>
					<div className="card-body w-responsive">
					<form onSubmit={(handleSubmit(Submit))}>
						<div className="container">
							<div className="form-group">
								<label>Title</label>
								<input
									className="form-control "
									value={form.title}
									type="text"
									id="title"
									name="title"
									placeholder="Masukan Title"
									onChange={(e) => handleChange(e)}
									ref={register({
										required: "This is required",
										minLength: {
											value: 3,
											message: "Min length is 3"
										}
									})}
								/>
								{errors.title && <p className="text-danger">{errors.title.message}</p>}
							</div>
							<div className="form-group">
								<label>Author</label>
								<input
									onChange={(e) => handleChange(e)}
									value={form.author}
									type="text"
									id="author"
									name="author"
									className="form-control"
									placeholder="Masukan Author"
									ref={register({
										required: "This is required",
										minLength: {
											value: 3,
											message: "Min length is 3"
										}
									})}
								/>
								{errors.author && <p className="text-danger">{errors.author.message}</p>}
							</div>
							<div className="form-group">
								<label>Published Date</label>
								<input
									onChange={(e) => handleChange(e)}
									value={moment(form.published_date).format('YYYY-MM-DD')}
									type="date"
									id="published_date"
									name="published_date"
									className="form-control"
									className="form-control"
									ref={register({
										required: "This is required"
									})}
								/>
								{errors.published_date && <p className="text-danger">{errors.published_date.message}</p>}
							</div>
							<div className="form-group">
								<label htmlFor="pages">Pages</label>
								<input
									id="pages"
									type="number"
									placeholder="Pages"
									name="pages"
									value={form.pages}
									onChange={(e) => handleChange(e)}
									className="form-control"
									ref={register({
										required: "This is required",
									})}
								/>
								{errors.pages && <p className="text-danger">{errors.pages.message}</p>}
							</div>
							<div className="form-group">
								<label>Language</label>
								<input
									onChange={(e) => handleChange(e)}
									value={form.language}
									type="text"
									id="language"
									name="language"
									className="form-control"
									placeholder="Masukan Language"
									ref={register({
										required: "This is required"
									})}
								/>
								{errors.language && <p className="text-danger">{errors.language.message}</p>}
							</div>
							<div className="form-group">
								<label>Publisher ID</label>
								<input
									onChange={(e) => handleChange(e)}
									value={form.publisher_id}
									type="text"
									id="publisher_id"
									name="publisher_id"
									className="form-control"
									placeholder="Masukan Publisher Id"
									ref={register({
										required: "This is required",
										minLength: {
											value: 3,
											message: "Min length is 3"
										}
									})}
								/>
								{errors.publisher_id && <p className="text-danger">{errors.publisher_id.message}</p>}
							</div>
							<button className="btn btn-primary">Submit</button>
						</div>
					</form>
					</div>
			</div>
			</div>
			</div>
				</div> :
				<App />
			}
		</>
	)
}

export default Create