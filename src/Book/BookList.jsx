import React, { useState, useEffect } from 'react'
import axios from 'axios'
import moment from 'moment'
import App from '../App'

const BookList = (props) => {

	const url = "http://127.0.0.1:8080/api/books/"
	const [book, setBook] = useState({ book: [] })
	const roles = sessionStorage.getItem("roles");


	useEffect(() => {
		axios.get(url,
			{
				headers: {
					"Authorization": sessionStorage.getItem('token')
				}
			}
		)
			.then(res => {
				console.log(res.data)
				setBook(res.data)
			}).catch(err => console.error(err))
	}, [])

	const Update = (id) => {
		console.log(id)
		props.history.push("/update/" + id)
	}

	const Remove = (id) => {
		const confirm = window.confirm("apakah anda yakin akan menghapus");
		if (confirm) {
			console.log(id)
			axios.delete(url + id, {
				headers: {
					"Authorization": sessionStorage.getItem('token')
				}
			})
				.then(res => {
					window.location.replace('/book')
				}).catch(err => console.error(err))
		}
	}

	const Tambah = () => {
		props.history.push('/addBook')
	}

	let no = 1
	const display = book.book.map(books =>
		<tr key={books.id}>
			<td>{no++}</td>
			<td>{books.title}</td>
			<td>{books.author}</td>
			<td>{moment(books.published_date).format('YYYY-MM-DD')}</td>
			<td>{books.pages}</td>
			<td>{books.language}</td>
			<td>{books.publisher_id}</td>
			<td><button onClick={() => Update(books.id)} className="btn btn-warning m-1">Update</button>
				|<button onClick={() => Remove(books.id)} className="btn btn-danger m-1">Delete</button></td>
		</tr>
	)

	return (
		<>
			{roles == 2 ? <div className="App">
				<div className="container">
					<br />
					<button style={{ float: "left" }} onClick={() => Tambah()} className="btn btn-info m-1">Tambah</button>
					<br />
					<br />
					<br />
					<table className="table table-striped">
						<thead class="thead-dark">
							<tr>
								<th>No</th>
								<th>Title</th>
								<th>Author</th>
								<th>Published Date</th>
								<th>Page</th>
								<th>Language</th>
								<th>Publisher Id</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							{display}
						</tbody>
					</table>
				</div>
			</div> :
				<App />
			}
		</>
	)
}

export default BookList