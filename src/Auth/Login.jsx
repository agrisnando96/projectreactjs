import React, { useState } from 'react';
import axios from 'axios';
import logo from '../logo.svg';


const SignIn = (props) => {

  const url = "http://127.0.0.1:8080/api/auth/signin"

  const [form, setForm] = useState({
    username: "",
    password: ""
  })

  const handleSubmit = (e) => {
    e.preventDefault()
    axios.post(url, form)
      .then(res => {
        if (res.status === 200) {
          sessionStorage.setItem("token", res.data.accessToken);
          sessionStorage.setItem("roles", res.data.roles);
          alert(
            // `Login Succes \n Your Acces Token is : ${sessionStorage.getItem('token')}`
            "login berhasil"
          )
          window.location.replace("/");
        } else {
          throw new Error('Login Failed')
        }
      })
      .catch(function (err) {
        console.log(err.response.status === 404)
        alert("Login Gagal")
      })
  }

  const handleChange = (e) => {
    const { name, value } = e.target
    setForm({
      ...form,
      [name]: value
    })
  }

  return (
    <div className="bg"
      style={{
        background: 'url(https://images2.alphacoders.com/840/thumb-1920-840511.jpg) center / cover',
        position: "relative",
        height: "610px",
        paddingTop: "100px"
      }}>
    <div className="row mt-5">
      <div className="container col-5">
        <div className="form-group">
          <div className="card">
            <div className="card-header">
              <h3>Sign In</h3>
            </div>
            <div className="card-body">
              <form onSubmit={(e) => handleSubmit(e)}>
                <div className="mb-2">
                  <label htmlFor="validationServer02">Username</label>
                  <input type="text"
                    className="form-control "
                    id="username"
                    onChange={(e) => handleChange(e)}
                    name="username"
                    required
                  />
                </div>
                <div className="mb-3">
                  <label htmlFor="validationServer02">Password</label>
                  <input
                    className="form-control "
                    id="password"
                    type="password"
                    name="password"
                    onChange={(e) => handleChange(e)}
                    required
                  />
                </div>
                <button className="btn btn-primary">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  )

}

export default SignIn