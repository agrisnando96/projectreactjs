import React, { useState, useEffect } from 'react'
import axios from 'axios'
import moment from 'moment'
import {
	Card, CardImg, CardBody, CardSubtitle,
	CardTitle, Button
} from 'reactstrap';

const BookList = (props) => {

	const url = "http://127.0.0.1:8080/api/books/"
	const [book, setBook] = useState({ book: [] })


	useEffect(() => {
		axios.get(url,
			{
				headers: {
					"Authorization": sessionStorage.getItem('token')
				}
			}
		)
			.then(res => {
				console.log(res.data)
				setBook(res.data)
			}).catch(err => console.error(err))
	}, [])

	const Detail = (id) => {
		console.log(id)
		props.history.push("/detail/" + id)
	}

	return (
		<div className="container mt-5">
			<h1>List Of Books</h1>
			<hr />
			<div className="row justify-content-between">
				{book.book.map(books => (
					<div key={books.id}>
						<Card style={{ width: "350px" }} className="mb-4">
							<CardImg top src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRaYd4ZbSFc0qeLyWmaI7utHlO1xpSjptkqMC9zfa4b4-1XeouP&s" alt="Card image cap" />
							<CardBody>
								<CardTitle>Title : {books.title}</CardTitle>
								<CardSubtitle >Author : {books.author}</CardSubtitle>
								<CardSubtitle>Language : {books.language}</CardSubtitle>
								<hr />
								<Button color="warning" onClick={() => Detail(books.id)}>Details</Button>
							</CardBody>
						</Card>
					</div>
				))}
			</div>
		</div>
	)
}

export default BookList