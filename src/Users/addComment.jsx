import React, { useState } from 'react'
import axios from 'axios'
import { withRouter } from 'react-router-dom'
import logo from '../logo.svg';

const Comment = (props) => {
    const url = "http://127.0.0.1:8080/api/comment/" + props.match.params.id

    const [comment, setComment] = useState({
        comment: ""
    })

    const handleSubmit = e => {
        axios.post(url, comment, {
            headers: {
                "Authorization": sessionStorage.getItem('token')
            }
        })
            .then(res => {
                const mydata = ({ ...comment })
                setComment(mydata)
            }).catch(err => console.error(err))
    }

    const handleChange = e => {
        const { name, value } = e.target
        setComment({
            ...comment,
            [name]: value
        })
    }

    return (
        <>
            <div className="container">
                <form onSubmit={(e) => handleSubmit(e)} className="p-5 bg-light">
                    <div class="form-group">
                        <label htmlFor="massage">Comment</label>
                        <textarea name="comment" id="comment" onChange={(e) => handleChange(e)}
                            value={comment.comment} className="form-control" required></textarea>
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Comment" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        </>
    )
}

export default withRouter(Comment)