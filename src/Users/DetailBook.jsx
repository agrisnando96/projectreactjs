import React, { useState, useEffect } from 'react';
import axios from 'axios';
import logo from '../logo.svg';
import {
    Card, CardImg, CardBody, CardSubtitle,
    CardTitle, Button
} from 'reactstrap';
import Comment from '../Users/addComment';

const Detail = props => {

    const url = "http://127.0.0.1:8080/api/detail/" + props.match.params.id;
    const [books, setBooks] = useState({
        data: [{
            title: "",
            author: "",
            pages: "",
            language: "",
            comments: [
                {
                    comment: "",
                    user: {
                        username: ""
                    }
                }
            ]
        }
        ]
    })

    useEffect(() => {
        axios.get(url, {
            headers: {
                "Authorization": sessionStorage.getItem('token')
            }
        })
            .then(res => {
                console.log(res.data.data)

                setBooks(res.data.data)
            }).catch(err => console.error(err))

    }, [])

    return (
        <div className="container mt-5">
            <h1>Detail Of Books</h1>
            <hr />
            <div className="row" key={books.id}>
                <div className="col-sm-8">
                    <p> Title of Book : {books.title}</p>
                    <p> Author of Book : {books.author}</p>
                    <p> Publised Date : {books.published_date}</p>
                    <p> Language of Book : {books.language}</p>
                    <p> Publisher Id of Book : {books.publisher_id}</p>
                    <hr />
                    <p>Description of the Book :</p>
                    <p>
                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Veniam
                        fugit incidunt facere quae vel quas fugiat, eveniet soluta. Quod
                        voluptates mollitia doloribus magni animi explicabo nesciunt debitis
                        obcaecati temporibus minus.
                    </p>
                </div>
                <div className="col-sm-4">
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRaYd4ZbSFc0qeLyWmaI7utHlO1xpSjptkqMC9zfa4b4-1XeouP&s" alt="Card image cap" alt="Book Image"
                        style={{ width: "350px", marginBottom: "50px" }}
                    />
                </div>
            </div>
            <Comment />
            <div className="row">
                {books.comments ?
                    <>
                        {books.comments.map((value, index) => (
                            <div className="col-sm-8" key={index}>
                                <hr />
                                <p>@{value.user.username}</p>
                                <p> Comment : {value.comment}</p>
                            </div>
                        ))}
                    </>
                    : ""
                }
            </div>
        </div>
    )
}

export default Detail
