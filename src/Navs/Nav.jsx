import React, { useState } from "react";
import { Button } from 'reactstrap';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  NavbarText
} from "reactstrap";

const Navs = props => {
  const [isOpen, setIsOpen] = useState(false);
  let Token = sessionStorage.getItem("token");
  let roles = sessionStorage.getItem("roles");


  const handleSignout = () => {
    Token = sessionStorage.clear();
    window.location.replace("/login/");
  };

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="dark" dark expand="md">
        <NavbarBrand href="/">Home</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          {Token ?
            <>
              {roles == 1
                ?
                <>
                  <Nav className="mr-auto" navbar>
                    <NavItem>
                      <NavLink href="/booklist/">Book List</NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink href="/profile/">Profile</NavLink>
                    </NavItem>
                  </Nav>
                  <NavbarText>
                    <NavLink href="#" onClick={handleSignout}><Button color="danger">Logout</Button>{' '}</NavLink>
                  </NavbarText>
                </> :
                <>
                  <Nav className="mr-auto" navbar>
                    <NavItem>
                      <NavLink href="/book/">Book</NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink href="/user/">User</NavLink>
                    </NavItem>
                  </Nav>
                  <NavbarText>
                    <NavLink href="#" onClick={handleSignout}><Button color="danger">Logout</Button>{' '}</NavLink>
                  </NavbarText>
                </>
              }
            </>
            :
            <>
              <nav className="ml-auto" navbar>
                <NavbarText>
                  <NavLink href="/login/"><button type="button" class="btn btn-danger btn-sm">Login</button></NavLink>
                </NavbarText>
                <NavbarText>
                  <NavLink href="/register/"><button type="button" class="btn btn-warning btn-sm">Register</button></NavLink>
                </NavbarText>
              </nav>
            </>
          }
        </Collapse>
      </Navbar>
    </div>
  );
};

export default Navs;
