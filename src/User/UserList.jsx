import React, { useState, useEffect } from 'react'
import axios from 'axios'
import moment from 'moment'

const UserList = (props) => {

    const url = "http://127.0.0.1:8080/api/users"
    const [user, setUser] = useState({ user: [] })
    const [role, setRole] = useState({ role: "" })

    useEffect(() => {
        axios.get(url,
            {
                headers: {
                    "Authorization": sessionStorage.getItem('token')
                }
            }
        )

            .then(res => {
                console.log(res.data)
                setUser(res.data)
            }).catch(err => console.error(err))
    }, [])

    const handleChange = (e) => {
        const { value } = e.target
        setRole({
            role: value
        })
    }

    const handleEdit = (usersId) => {
        const data = {
            roleId: role.role
        }
        axios.put(`http://127.0.0.1:8080/api/users/${usersId}`, data, {
            headers: {
                "authorization": sessionStorage.getItem('token')
            }
        })
            .then(res => {
                console.log(res)
                alert("Value berhasil di ubah")
                window.location.replace('/user')
            })
            .catch(er => {
                console.log(er)
            })
    }

    let no = 1
    const display = user.user.map(users =>
        <tr key={users.id}>
            <td>{no++}</td>
            <td>{users.name}</td>
            <td>{users.username}</td>
            <td>{users.email}</td>
            {/* {users.roles.map((role, index) => (
                <td key={index}>{role.name}</td>
            ))} */}
            <td><select class="browser-default custom-select" name='role' onChange={handleChange}>
                <option value={users.roles[0].id}>
                    {users.roles[0].name}
                </option>
                <option value='1'>
                    USER
                </option>
                <option value='2'>
                    ADMIN
                </option>
            </select></td>
            <td><button onClick={() => { handleEdit(users.id) }} className="btn btn-info m-1">Update</button></td>
        </tr>
    )

    return (
        <div className="App">
            <div className="container">
                <br />
                <h1>Data All Users</h1>
                <br />
                <table className="table table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {display}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default UserList