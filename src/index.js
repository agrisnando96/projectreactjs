import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Nav from './Navs/Nav';
import BookList from './Book/BookList';
import UserList from './User/UserList';
import ViewBook from './Users/Viewbook';
import DetailBook from './Users/DetailBook';
import Create from './Book/AddBook';
import Update from './Book/UpdateBook';
import GlobalHook from './GlobalHook/GlobalHook';
import Login from './Auth/Login';
import Register from './Auth/Register';
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import Detail from './Users/DetailBook';
import Profile from './Users/Profile';


const routing = (
    <Router>
        <Nav />
        <Switch>
            <Route exact path="/" component={App} />
            <Route path="/book" component={BookList} />
            <Route path="/user" component={UserList} />
            <Route path="/booklist" component={ViewBook} />
            <Route path="/profile" component={Profile} />
            <Route path="/detail/:id" component={Detail} />
            <Route path="/addBook" component={Create} />
            <Route path="/update/:id" component={Update} />
            <Route path="/login/" component={Login} />
            <Route path="/register/" component={Register} />
            <Route path="/globalhook" component={GlobalHook} />
        </Switch>
    </Router>
);
ReactDOM.render(routing, document.getElementById("root"));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
